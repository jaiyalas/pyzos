# pyzos

The **Pyzos** is a python-based demo language for smart contract on Tezos. The **pyzos** is a simple translator from a restricted python-based language, Pyzos, into the Michelson.

## Build and Usage

Build

> stack build

Print output on screen

> stack exec -- pyzos-exe Gen -i \</path/inputfile\>

Print output on file

> stack exec -- pyzos-exe Gen -i \</path/inputfile\> -o \</path/outputfile\>

## Major phases

### [1] python syntax parsing

A Pyzos program contains only legal python syntax so it's natural to use python parser to process at beginning.

- Applying a pure python syntax parser to obtain a legit python AST (abstract syntax tree).

**May Raise**: [PARSE ERROR] - input is not even a legal python syntax.

### [2] syntax pre-processing

- Defined internal operating data structures, in DataP.hs, for simplifying the input python AST
and for further manipulation.
- Applying some syntax auto-fulfillment.
   - `entry():` => `entry(_ : tez.Unit) -> tez.Unit:`
   - `10` => `return 10`
- Restricting some syntax we disallow or unsupported (for now)

**May Raise**: [UNSUPPORTED PYTHON SYNTAX ERROR] - used some syntax Pyzos doesn't support (yet).

**todo**

- [ ] seperating `BinaryOp` into several group
- [ ] rewriting `tez.tez(n :: int)` by `tez.tez(n :: nat)`
- [ ] making all `EzAssign` into `EzAnnoAssign`

### [3] Pyzos-focused syntax checking

There are tons of things can/should be done here. We just write some very primitive checks.

- Pyzos program must be written in several specific structures / layouts. (Actually, we allow only one layout)
- The explicit type annotation can appear only on storage, parameters, variable declaration.
- Please read *ProgrammingGuide.md* for more details.

**May Raise**: [SYNTAX ERROR] - used Pyzos syntax in the wrong way.

**todo**

- [ ] `pass` must be stand-along within one code-block
- [ ] variable cannot be started with `"_in_"`
- [x] storage must take a list of types
- [ ] `pass` should appear only in branch
- [ ] check absence of non-tez dot
- [x] return on the end of all statements
- [ ] `tez.nat(n)` where `n >= 0`
- [ ] `tez.tez(n)` where `n >= 0`
- [ ] To fuse separated checking functions into one
- [ ] Try general traversal again

### [4] Pyzos-/Michelson-based type checking

- Type-inference based checking
- Using context (via manual reader monad)
- Using type variable for polymorphic-ish type matching
- Defined type terms based-on Michelson's types
- Make sure Pyzos is well-typed

**May Raise**: [TYPE ERROR] - wrong type in wrong position.

**todo**

- [ ] Condition of `IF` should take `maybe a` and `either a b`

### [5] Pyzos-to-Michelson translation

- Define internal structure, in DataM.hs , for representing Michelson program.
- Maintaining an environment for manipulating data, variables and code fragments during program conversion.
- Design an stack for mimicking stack in Michelson
- Implement the linearity of Variables  

**todo**

- [ ] More supporting on
   - [ ] `Nat`, `Option`, `Or`
   - [ ] `Pair`, `List`
- [ ] Optimization
   - [ ] reducing the size of Michelson program
   - [ ] reducing the amount of use of `SWAP`, `DUP`, etc..
