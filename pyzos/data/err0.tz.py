storage: [tez.Unit]

def entry(_i : tez.Int, _t : tez.Tez):
    x = _t + tez.tez(_i)
    x * tez.tez(5)

# .....[Preprocessed]
# .....[Syntax Checked]
# [TYPE ERROR] Is Untypeable
# 	Cannot reduce a type from the function body of: entry
# [TYPE ERROR] Cannot Match Types
# 	From: EzBinaryOp {operator = EzMultiply, leftOpArg = EzVar "x", rightOpArg = EzCall {callFun = EzTez "tez", callArgs = [EzArgExpr {argExpr = EzInt {intValue = 5, exprLiteral = "5"}}]}}
# 	[EXPECTED]: TezTypeUnit
# 	[ACTUALLY]: TezTypeTez
