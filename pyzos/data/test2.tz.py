storage: [tez.Unit]

def entry(_i : tez.Int) -> tez.Int:
    if _i >= 0 : _i
    else : tez.fail


#opam@ad97f864a278:~$ tezos-client typecheck program ./sample/test2.tz
#Well typed

#opam@ad97f864a278:~$ tezos-client run program ./sample/test2.tz  on storage Unit and input '1'
#storage
#  Unit
#output
#  1
