storage: [tez.Tez]

def entry(_x : tez.Tez) -> tez.Tez:
    if tez.amount > tez.balance :
        _x
    elif tez.amount > _x :
        _y = _x + _x
    else:
        tez.tez(0)

#opam@ad97f864a278:~$ tezos-client typecheck program ./sample/test0.tz
#Well typed

#opam@ad97f864a278:~$ tezos-client run program ./sample/test0.tz  on storage \"4\" and input "\"3\"" -amount '99.00'
#storage
#  "4"
#output
#  "6"
