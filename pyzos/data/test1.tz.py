storage: [tez.Tez]

def entry():
    if tez.balance > tez.tez(0) :
        return tez.unit()
    else:
        tez.fail()

#opam@ad97f864a278:~$ tezos-client typecheck program ./sample/test1.tz
#Well typed


#opam@ad97f864a278:~$ tezos-client run program ./sample/test1.tz  on storage \"4\" and input Unit
#storage
#  "4"
#output
#  Unit
