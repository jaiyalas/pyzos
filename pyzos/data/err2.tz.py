storage: [tez.Unit]

def entry(x : tez.Tez):
    if (x >= tez.unit) :
        x
    else:
        x * 5

# .....[Preprocessed]
# .....[Syntax Checked]
# [TYPE ERROR] Is Untypeable
# 	Cannot reduce a type from the function body of: entry
# [TYPE ERROR] Types Inconsistent
# 	Branches:
# 	  ErrorType	->  ErrorType
# 	  <else>	->  ErrorType
# 	are inconsistent
# [TYPE ERROR] Cannot Match Types
# 	From: EzBinaryOp {operator = EzMultiply, leftOpArg = EzVar "x", rightOpArg = EzInt {intValue = 5, exprLiteral = "5"}}
# 	[EXPECTED]: TezTypeUnit
# 	[ACTUALLY]: ErrorType
# [TYPE ERROR] Types Inconsistent
# 	Types
# 	  "TezTypeTez"
# 	and
# 	  "TezTypeInt"
# 	are inconsistent for EzMultiply
# [TYPE ERROR] Is NOT a Boolean
# 	From: EzBinaryOp {operator = EzGreaterThanEquals, leftOpArg = EzVar "x", rightOpArg = EzCall {callFun = EzTez "unit", callArgs = []}}
# 	is not a boolean
# [TYPE ERROR] Types Inconsistent
# 	Types
# 	  "TezTypeTez"
# 	and
# 	  "TezTypeUnit"
# 	are inconsistent for EzGreaterThanEquals
