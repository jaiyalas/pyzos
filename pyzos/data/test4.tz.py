storage: [tez.Unit]

def entry(x : tez.Int) -> tez.Int:
    return (x * x * 100)


#opam@ad97f864a278:~$ tezos-client typecheck program ./sample/test4.tz
#Well typed

#opam@ad97f864a278:~$ tezos-client run program ./sample/test4.tz  on storage Unit and
#input "-2"
#storage
#  Unit
#output
#  400
