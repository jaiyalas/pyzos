storage: [tez.Unit]

def entry(x : tez.Tez):
    if (x >= tez.Unit) :
        x
    else:
        x * tez.int(5)

# .....[Preprocessed]
# [SYNTAX ERROR] Should NOT be a Type Term
# 	EzTezT "Unit"
