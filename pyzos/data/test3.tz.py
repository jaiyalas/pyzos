storage: [tez.Tez]

def entry(_t : tez.Tez, _i : tez.Int) -> tez.Tez:
    y = storage - tez.tez(10)
    if _t >= y :
        tez.tez(100)
    elif (_t >= storage) && (_t >= tez.tez(0)) :
        return _t
    else:
        tez.fail

#opam@ad97f864a278:~$ tezos-client typecheck program ./sample/test3.tz
#Well typed

#opam@ad97f864a278:~$ tezos-client run program ./sample/test3.tz  on storage "\"30\"" and input "(Pair \"50.00\" 2)"
#storage
#  "30"
#output
#  "100"
