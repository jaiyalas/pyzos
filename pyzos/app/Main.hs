module Main where
--
import Pyzos
--
import Data.Maybe
import Control.Monad (mapM)
import Control.Monad.State (runState)
--
import System.IO
import Text.Pretty.Simple (pPrint)
import Language.Python.Version3
import qualified Options.Applicative as O

main :: IO ()
main = do
    opts <- O.execParser $ O.info (O.helper <*> cmdLineOut) O.fullDesc
    case opts of
      Gen {} -> gen opts
--
gen :: Arg -> IO ()
gen opts = do
   let o = outputFile opts
   let i = inputFile opts
   c <- readFile i
   let result = parseModule c i
   case result of
      (Left msg) -> do
         putParseError msg
      (Right (mspan, tks)) -> do
         mezm <- process (proceedPreprocess mspan)
         -- pPrint mezm
         case mezm of
            Nothing -> return ()
            (Just (EzModule ex)) -> convOut o ex
--
convOut :: Maybe String -> [EzStatement] -> IO ()
convOut o ex = do
   let (storage, eS) = metaInfo_ ex $ \x -> toStorage x initEnv
   let (param, eP)   = metaInfo_ ex $ \x -> toParam x eS [CAR]
   let eB = eP { stackBias = stackBias eP - 1 }
   let ret     = metaInfo ex toReturn
   let (cod, eC)     = metaInfo_ ex $ \x -> toCode x eB
   let sc = toMichelson $ SC param ret storage cod
   case o of
      Just p -> do
         h <- openFile p WriteMode
         putStrLn $ "Output File:" ++ p
         hPutStrLn h sc
         hClose h
      Nothing ->
         putStrLn sc
