{-# LANGUAGE GADTs              #-}
{-# LANGUAGE StandaloneDeriving #-}

module Pyzos.Data.DataM where

import Data.Monoid
import Data.List
import Data.Char

class (Show a) => ToMichelson a where
    toMichelson :: a -> String
    toMichelson = lazy2MichelsonTp

data MComparableType where
    MTpString :: MComparableType
    MTpInt    :: MComparableType
    MTpBool   :: MComparableType
    MTpNat    :: MComparableType
    MTpTez    :: MComparableType
    deriving (Show, Eq)

instance ToMichelson MComparableType

data MType where
    MTpUnknow         :: MType
    MTpUnit           :: MType
    MTpOption         :: MType -> MType
    MTpPair           :: MType -> MType -> MType
    MTpOr             :: MType -> MType -> MType
    MTpComparableType :: MComparableType -> MType
    deriving (Show, Eq)

instance ToMichelson MType where
    toMichelson (MTpOption v) = "option" <>. pt (toMichelson v)
    toMichelson (MTpPair v w) = "pair" <>. pt (toMichelson v) <>. pt (toMichelson w)
    toMichelson (MTpOr v w)   = "or" <>. pt (toMichelson v) <>. pt (toMichelson w)
    toMichelson (MTpComparableType c) = lazy2MichelsonTp c
    toMichelson x = lazy2MichelsonTp x

data Nat where
    Zero :: Nat
    Succ :: Nat -> Nat
    deriving (Show, Eq)

instance ToMichelson Nat where
  toMichelson = show . fromNat

data Lit a where
    LInt :: Integer -> Lit Integer
    LStr :: String -> Lit String

deriving instance Show (Lit a)
deriving instance Eq (Lit a)

instance ToMichelson (Lit a) where
    toMichelson (LInt x) | x >= 0 =  show x
                         | otherwise = "(" <> show x <> ")"
    toMichelson (LStr x) = "\"" <> x <> "\""

data DataM where
    MInt     :: Lit Integer -> DataM
    MString  :: Lit String -> DataM
    MTez     :: Lit String -> DataM
    MNat     :: Nat -> DataM
    MSome    :: DataM -> DataM
    MLeft    :: DataM -> DataM
    MRight   :: DataM -> DataM
    MUnit    :: DataM
    MTrue    :: DataM
    MFalse   :: DataM
    MNone    :: DataM
    MInstruction :: [Instruction] -> DataM
    deriving (Show, Eq)

instance ToMichelson DataM where
    toMichelson (MInt x) = toMichelson x
    toMichelson (MString x) = toMichelson x
    toMichelson (MTez x) = toMichelson x
    toMichelson (MNat x) = toMichelson x
    toMichelson (MSome x) = pt $ "Some" <>. toMichelson x
    toMichelson (MLeft x) = pt $ "Left" <>. toMichelson x
    toMichelson (MRight x) = pt $ "Right" <>. toMichelson x
    toMichelson (MInstruction x) = intercalate "; " $ map toMichelson x
    toMichelson x = lazy2MichelsonDt x
data Instruction = PUSH MType DataM
                 | LEFT MType
                 | RIGHT MType
                 | NONE MType
                 | DIP Int DataM
                 | IF_NONE DataM DataM
                 | IF_LEFT DataM DataM
                 | IF DataM DataM
                 | EMPTY_
                 | DROP
                 | DUP
                 | SWAP
                 | SOME
                 | UNIT
                 | PAIR
                 | CAR
                 | CDR
                 | FAIL
                 | CONCAT
                 | ADD
                 | SUB
                 | MUL
                 | DIV
                 | ABS
                 | NEG
                 | MOD
                 | LSL
                 | LSR
                 | OR
                 | AND
                 | XOR
                 | NOT
                 | COMPARE
                 | EQ
                 | NEQ
                 | LT
                 | GT
                 | LE
                 | GE
                 | INT
                 | NOW
                 | AMOUNT
                 | BALANCE
                 | CMPEQ
                 | CMPNEQ
                 | CMPLT
                 | CMPGT
                 | CMPLE
                 | CMPGE
                 deriving (Show, Eq)

instance ToMichelson Instruction where
    toMichelson (PUSH t d) = "PUSH" <>. toMichelson t <>. toMichelson d
    toMichelson (LEFT t) = "LEFT" <>. toMichelson t
    toMichelson (RIGHT t) = "RIGHT" <>. toMichelson t
    toMichelson (NONE t) = "NONE" <>. toMichelson t
    toMichelson (DIP n t) = "D" <> replicate n 'I' <> "P"
                                <>. cb (toMichelson t)
    toMichelson (IF_NONE s t) = "IF_NONE" <>. binaryOpCb s t
    toMichelson (IF_LEFT s t) = "IF_LEFT" <>. binaryOpCb s t
    toMichelson (IF s t) = "IF" <>. binaryOpCb s t
    toMichelson EMPTY_ = ""
    toMichelson x = show x

data SmartConstract = SC { param   :: MType
                         , retun  :: MType
                         , storage :: MType
                         , code    :: DataM
                         }
                         deriving (Show)

instance ToMichelson SmartConstract where
    toMichelson (SC p r s c)
      = "parameter" <>. end (pt (toMichelson p)) <> "\n"
      <> "return" <>. end (pt (toMichelson r)) <> "\n"
      <> "storage" <>. end (pt (toMichelson s)) <> "\n"
      <> "code" <>. end (cb (toMichelson c))


lazy2MichelsonTp :: (Show a)
               => a
               -> String
lazy2MichelsonTp = drop 3 . map toLower . show

lazy2MichelsonDt :: (Show a)
               => a
               -> String
lazy2MichelsonDt = drop 1 . show

(<>.) :: String -> String -> String
(<>.) x y = x <> " " <> y

pt :: String -> String
pt x = "(" <> x <> ")"

cb :: String -> String
cb x = "{" <> x <> "}"

end :: String -> String
end x = x <> ";"

binaryOpCb :: (ToMichelson a, ToMichelson b) => a -> b -> String
binaryOpCb s t = cb (toMichelson s) <>. cb (toMichelson t)

toNat :: (Integral a) => a -> Nat
toNat 0 = Zero
toNat a = Succ (toNat (a - 1))

fromNat :: (Integral a) => Nat -> a
fromNat Zero     = 0
fromNat (Succ a) = 1 + fromNat a

addInst :: DataM -> DataM -> DataM
addInst (MInstruction []) my@(MInstruction y) = my
addInst mx@(MInstruction x) (MInstruction y)
  = if last x == FAIL then
       mx
    else
       MInstruction $ x ++ y
addInst _ _ = error $ show "Not a MInstruction. Cannot be added."

instToDataM :: Instruction -> DataM
instToDataM  = MInstruction . (:[])
