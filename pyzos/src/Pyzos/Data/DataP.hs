module Pyzos.Data.DataP where
--
import Language.Python.Common.AST
--
import Data.Char
import Data.Maybe (maybe)
import Data.List (null)
--
newtype EzModule
   = EzModule [EzStatement]
   deriving (Show, Eq)
--
type EzSuite = [EzStatement]
--
data EzStatement
   = EzFun
      { funName :: String
      , funArgs :: EzParameter
      , funResultAnnotation :: EzExpr
      , funBody :: EzSuite
      }
   | EzAssign{ assignTo :: [EzExpr], assignExpr :: EzExpr }
   | EzAnnotatedAssign { annAssignAnnotation :: EzExpr
                       , annAssignTo :: EzExpr
                       , annAssignExpr :: Maybe EzExpr
                       }
   | EzTezStorage { storageType :: EzExpr }
   | EzStmtExpr{ stmtExpr :: EzExpr }
   | EzConditional{ condGuards :: [(EzExpr, EzSuite)], condElse :: EzSuite }
   | EzReturn{ returnExpr :: EzExpr }
   --
   | EzPass
   --
   deriving (Show, Eq)
--
--
data EzParameter
   = EzParam (String, EzExpr)
   | EzParamPair { lp :: EzParameter, rp :: EzParameter}
   deriving (Eq, Show)
--
data EzExpr
   = EzVar String
   | EzInt { intValue :: Integer, exprLiteral :: String }
   | EzBool { boolValue :: Bool }
   | EzString { stringsString :: String }
   | EzCall { callFun :: EzExpr, callArgs :: [EzArgument] }
   | EzCondExpr { ceTrueBranch :: EzExpr
                , ceCondition :: EzExpr
                , ceFalseBranch :: EzExpr
                }
   | EzBinaryOp { operator :: EzOp, leftOpArg :: EzExpr, rightOpArg :: EzExpr }
   | EzUnaryOp { operator :: EzOp, opArg :: EzExpr }
   | EzDot { dotExpr :: EzExpr, dotAttribute :: String }
   | EzPairT { le :: EzExpr, re :: EzExpr }
   | EzParen { parenExpr :: EzExpr }
   --
   | EzTez String
   | EzTezT String
   deriving (Show, Eq)
--
data EzArgument
   = EzArgExpr {argExpr :: EzExpr}
   deriving (Show, Eq)
--
data EzOp
   = EzLessThan          -- ^ \'<\'
   | EzGreaterThan       -- ^ \'>\'
   | EzEquality          -- ^ \'==\'
   | EzGreaterThanEquals -- ^ \'>=\'
   | EzLessThanEquals    -- ^ \'<=\'
   | EzNotEquals         -- ^ \'!=\'
   --
   | EzShiftLeft  -- ^ \'<<\'
   | EzShiftRight -- ^ \'>>\'
   | EzBinaryOr   -- ^ \'|\'
   | EzBinaryAnd  -- ^ \'&\'
   | EzXor        -- ^ \'^\'
   --
   | EzMultiply -- ^ \'*\'
   | EzPlus     -- ^ \'+\'
   | EzMinus    -- ^ \'-\'
   | EzDivide   -- ^ \'\/\'
   --
   | EzAnd   -- ^ \'and\' == \'&&\'
   | EzOr    -- ^ \'or\' == \'||\'
   | EzNot   -- ^ \'not\'
   deriving (Show, Eq)
--
-- | AST translation: from language-python (AST) into pyzos (EzAST).
-- Some optimizations and autofilling operations are included.
ezModule :: (Show a) => Module a -> Maybe EzModule
ezModule (Module xs) = do
   xs' <- mapM ezStatement xs
   return $ EzModule xs'
--
ezStatement :: (Show a) => Statement a -> Maybe EzStatement
-- special: storage handling
ezStatement (AnnotatedAssign x@(List xs _) y@(Var (Ident name _) _) Nothing _) =
   if name == "storage"
      then mapM ezExpr xs >>= ezPairizingEzExpr >>= (return.EzTezStorage)
      else do
         x' <- ezExpr x
         y' <- ezExpr y
         return $ EzAnnotatedAssign x' y' Nothing
-- special: filling void input-/return-type with unit
ezStatement (Fun (Ident x _) ys z w _) = do
   argType <- if (null ys)
                  then return $ EzParam ("_", EzTezT "Unit")
                  else ezPairizingParam ys
   retType <- maybe (return $ EzTezT "Unit") (ezExpr) z
   w' <- mapM ezStatement w
   return $ EzFun x argType retType w'
-- regular
ezStatement (Assign xs y _) = do
   xs' <- mapM ezExpr xs
   y' <- ezExpr y
   return $ EzAssign xs' y'
ezStatement (AnnotatedAssign x y z _) = do
   z' <- mapM ezExpr z
   y' <- ezExpr y
   x' <- ezExpr x
   return $ EzAnnotatedAssign x' y' z'
ezStatement (StmtExpr x _) =
    ezExpr x >>= (return . EzStmtExpr)
ezStatement (Conditional xs ys _) = do
   ys' <- mapM ezStatement ys
   xs' <- mapM (\(a, b)-> do
                  a' <- ezExpr a
                  b' <- mapM ezStatement b
                  return (a', b') ) xs
   return $ EzConditional xs' ys'
ezStatement (Return x _) =
   maybe (return $ EzTez "unit") ezExpr x >>= (return.EzReturn)
ezStatement (Pass _) = return EzPass
ezStatement s = Nothing
--
--
ezParameter :: (Show a) => Parameter a -> Maybe EzParameter
-- special
ezParameter (Param (Ident x _) (Just y) z _) = do
   y' <- ezExpr y
   return $ EzParam (x, y')
ezParameter p = Nothing
--
--
ezExpr :: (Show a) =>  Expr a -> Maybe EzExpr
-- optimization
ezExpr (Dot x@(Var (Ident name annotI) annotV) y@(Ident attr annotI2) annotD) =
   case (name, isUpper $ head attr) of
      ("tez", True) -> return $ EzTezT attr
      ("tez", False) -> return $ EzTez attr -- EzTez attr
      _ -> do
         x' <- ezExpr x
         return $ EzDot x' attr
ezExpr (Tuple [a,b] _) = do
   a' <- ezExpr a
   b' <- ezExpr b
   return $ EzPairT a' b'
-- regular
ezExpr (Var (Ident x _) _) = return $ EzVar x
ezExpr (Int x y _) = return $ EzInt x y
ezExpr (Bool x _) = return $ EzBool x
ezExpr (Strings xs _) = return $ EzString $ concat xs
ezExpr (Call x ys _) = do
   x' <- ezExpr x
   ys' <- mapM ezArgument ys
   return $ EzCall x' ys'
ezExpr (CondExpr x y z _) = do
   x' <- ezExpr x
   y' <- ezExpr y
   z' <- ezExpr z
   return $ EzCondExpr x' y' z'
ezExpr (BinaryOp x y z _) = do
   x' <- ezOp x
   y' <- ezExpr y
   z' <- ezExpr z
   return $ EzBinaryOp x' y' z'
ezExpr (UnaryOp x y _) = do
   x' <- ezOp x
   y' <- ezExpr y
   return $ EzUnaryOp x' y'
ezExpr (Dot  x (Ident y _) _) = do
   x' <- ezExpr x
   return $ EzDot x' y
ezExpr (Paren x _) = ezExpr x
ezExpr e = Nothing
--
--
ezArgument :: (Show a) => Argument a -> Maybe EzArgument
ezArgument (ArgExpr e _) = ezExpr e >>= (return . EzArgExpr)
--
--
ezOp :: (Show a) => Op a -> Maybe EzOp
ezOp (LessThan _)    = return $ EzLessThan
ezOp (GreaterThan _) = return $ EzGreaterThan
ezOp (Equality _)    = return $ EzEquality
ezOp (GreaterThanEquals _) = return $ EzGreaterThanEquals
ezOp (LessThanEquals _)    = return $ EzLessThanEquals
ezOp (NotEquals _)         = return $ EzNotEquals
ezOp (BinaryOr _)  = return $ EzBinaryOr
ezOp (BinaryAnd _) = return $ EzBinaryAnd
ezOp (Xor _)       = return $ EzXor
ezOp (Multiply _) = return $ EzMultiply
ezOp (Plus _)     = return $ EzPlus
ezOp (Minus _)    = return $ EzMinus
ezOp (Divide _)   = return $ EzDivide
ezOp (And _) = return $ EzAnd
ezOp (Or _)  = return $ EzOr
ezOp (Not _) = return $ EzNot
ezOp op = Nothing
--
-- | restructure list of expr as pair of expr
ezPairizingEzExpr :: [EzExpr] -> Maybe EzExpr
ezPairizingEzExpr [a]    = rmTypeCall a
ezPairizingEzExpr [a, b] = do
   a' <- rmTypeCall a
   b' <- rmTypeCall b
   return $  EzPairT a' b'
ezPairizingEzExpr (x:xs) =
   ezPairizingEzExpr xs >>= (return . EzPairT x)
ezPairizingEzExpr x = Nothing
--
rmTypeCall :: EzExpr -> Maybe EzExpr
rmTypeCall (EzCall (EzTezT "EzPairT") [EzArgExpr e1, EzArgExpr e2]) = do
   e1' <- ezPairizingEzExpr [e1]
   e2' <- ezPairizingEzExpr [e2]
   return $ EzPairT e1' e2'
rmTypeCall a = return a

-- | restructure list of parameter as pair of parameter
ezPairizingParam :: (Show a) => [Parameter a] -> Maybe EzParameter
ezPairizingParam [p]    = ezParameter p
ezPairizingParam (p:ps) = do
   ps' <- ezPairizingParam ps
   p' <- ezParameter p
   return (EzParamPair p' ps')
ezPairizingParam [] = Nothing
--
