
module Pyzos.Data.Converter where

import Pyzos.Data.DataP
import Pyzos.Data.DataM as M

import Control.Monad

import Data.Maybe
import Data.HashMap.Lazy as H

data Cat = LOCAL | TEMP | PARAM | STORAGE | AMT | BAL
               deriving (Show, Eq)
type VarName = String

tmpVar :: VarName
tmpVar = "_in_tmpVar"

data VarInfo = VI { vName :: VarName
                  , typeM :: MType
                  , dataM :: DataM
                  , scope :: Cat
                  } deriving Show

data RawVarInfo = RV { vNameR :: VarName
                     , expr  :: EzExpr
                     , scopeR :: Cat
                     } deriving Show

data Env = Env { stack     :: [VarInfo]
               , stackBias :: Int
               , vars      :: HashMap VarName RawVarInfo }
               deriving Show

initEnv :: Env
initEnv = Env [] 0 H.empty

typeP2M :: HashMap String MType
typeP2M = fromList
        [ ("Unit"  , MTpUnit)
        {-, ("Option", MTpOption)-}
        {-, ("Pair"  , MTpPair)-}
        {-, ("Or"    , MTpOr)-}
        , ("String", MTpComparableType MTpString)
        , ("Int"   , MTpComparableType MTpInt)
        , ("Bool"  , MTpComparableType MTpBool)
        , ("Nat"   , MTpComparableType MTpNat)
        , ("Tez"   , MTpComparableType MTpTez)
        ]

metaInfo :: (ToMichelson a) => [EzStatement] -> (EzStatement -> Maybe a) -> a
metaInfo ex f = fromJust $ head $ Prelude.filter isJust $ Prelude.map f ex

metaInfo_ :: (ToMichelson a) => [EzStatement] -> (EzStatement -> (Maybe a, Env)) -> (a, Env)
metaInfo_ ex f = (fromJust (fst a), snd a)
    where a = head $ Prelude.filter (isJust.fst) $ Prelude.map f ex

toStorage :: EzStatement -> Env -> (Maybe MType, Env)
toStorage (EzTezStorage x) e = (Just t, e'')
                             where t = ezExprToType x
                                   v = VI "storage" t (MInstruction [M.CDR]) STORAGE
                                   e' = e { stackBias
                                            = 1 + stackBias e}
                                   e'' = pushVar v $ flip storeVar e' $ storageRaw "storage" x
toStorage _ e = (Nothing, e)

toParam :: EzStatement -> Env -> [Instruction] -> (Maybe MType, Env)
toParam (EzFun _ x _ _) e i = toParam' x e i
toParam _ e i = (Nothing, e)

toParam' :: EzParameter -> Env -> [Instruction] -> (Maybe MType, Env)
toParam' (EzParam (s, v)) e d = (Just t, e'')
                         where t = ezExprToType v
                               var = VI s t (MInstruction d) PARAM
                               e' = e { stackBias
                                        = 1 + stackBias e}
                               e'' = pushVar var $ flip storeVar e'
                                                 $ paramRaw s v
toParam' (EzParamPair p1 p2) e d = (t, e2)
             where (t1,e1) =  toParam' p1 e (d ++ [M.CAR])
                   (t2,e2) =  toParam' p2 e1 (d ++ [M.CDR])
                   t = MTpPair <$> t1 <*> t2

toReturn :: EzStatement -> Maybe MType
toReturn (EzFun _ _ x _) = Just $ ezExprToType x
toReturn _ = Nothing

ezExprToType :: EzExpr -> MType
ezExprToType (EzPairT a b) = MTpPair (ezExprToType a) (ezExprToType b)
ezExprToType (EzTezT t) = typeP2M ! t
ezExprToType e = error $ show e

{-# ANN toCode ("HLint: ignore Use section" :: String )#-}
toCode :: EzStatement -> Env -> (Maybe DataM, Env)
toCode (EzFun _ _ _ x) e = runEzSuite x e
toCode _ e = (Nothing, e)

toCode' :: EzStatement -> Env -> (Maybe DataM, Env)
toCode' (EzConditional xs y) e = ezCondToDataM xs y e
toCode' (EzReturn x) e =
      (z, e3)
      where (d, e') = ezExprToDataM x e
            (d1, e1, b) = dupVar "storage" e'
            (d2, e2) = mvVar "storage" e1
            e3 = popVar $ popVar e2
            dEnd = addInst (instToDataM M.SWAP) (instToDataM M.PAIR)
            dDrp = instToDataM $ DIP 1 (instToDataM M.DROP)
            s = addInst <$> d <*> d1
            t = addInst <$> s <*> d2
            v | b = addInst <$> t <*> Just (instToDataM M.CDR)
              | otherwise = t
            w = addInst <$> v <*> pure dEnd
            z = addInst <$> w <*> pure dDrp

toCode' (EzStmtExpr x) e = ezExprToDataM x e
toCode' (EzAssign x v) e
   = (Just $ MInstruction [], ezAssignToDataM x v e)
toCode' x e = error $ show x

runEzSuite :: EzSuite -> Env -> (Maybe DataM, Env)
runEzSuite [] e = (Just $ MInstruction[], e)
runEzSuite (x:xs) e = (addInst <$> d1 <*> d2, e2)
                      where (d1, e1) = toCode' x e
                            (d2, e2) = runEzSuite xs e1

ezAssignToDataM :: [ EzExpr ] -> EzExpr -> Env -> Env
ezAssignToDataM (EzVar x:xs) v@(EzTez a@"amount") e
   = ezAssignToDataM xs v $ storeVar (amountRaw x v) e
ezAssignToDataM (EzVar x:xs) v@(EzTez a@"balance") e
   = ezAssignToDataM xs v $ storeVar (balanceRaw x v) e
ezAssignToDataM (EzVar x:xs) v@(EzCall (EzTez "tez") z) e
   = ezAssignToDataM xs v $ storeVar (tezRaw x v) e
ezAssignToDataM (EzVar x:xs) v@(EzVar y) e
   = ezAssignToDataM xs v $ storeVar (localRaw x v) e
ezAssignToDataM (EzVar x:xs) v@(EzBinaryOp o l r) e
   = ezAssignToDataM xs v $ storeVar (localRaw x v) e
ezAssignToDataM _ _ e = e

lookupVar :: VarName -> Env -> Maybe RawVarInfo
lookupVar n e = H.lookup n (vars e)

{-# ANN ezCondToDataM ("HLint: ignore Use foldr" :: String )#-}
ezCondToDataM :: [(EzExpr, EzSuite)] -> EzSuite -> Env -> (Maybe DataM, Env)
ezCondToDataM [] y e = runEzSuite y e
ezCondToDataM ((a,b):xs) y e =
    (join $ f <$> d1 <*> d2, e2)
    where f :: DataM -> DataM -> Maybe DataM
          f s t = addInst s . instToDataM <$> (i <*> pure t)
          i :: Maybe (DataM -> Instruction)
          i = IF <$> d3
          (d1, e1) = ezExprToDataM a e
          e2 = popVar e1
          (d2, e3) = ezCondToDataM xs y e2
          (d3, e4) = runEzSuite b e2

{-# ANN ezExprToDataM ("HLint: ignore Use head" :: String )#-}
ezExprToDataM :: EzExpr -> Env -> (Maybe DataM, Env)
ezExprToDataM (EzBinaryOp o l r) e = (addInst <$> t <*> u, e5)
                where (d1, e1) = ezExprToDataM l e
                      (d2, e2) = ezExprToDataM r e1
                      fstStackVar = stack e1 !! 0
                      sndStackVar = stack e2 !! 0
                      (d3, e3) = binaryOpOrder fstStackVar sndStackVar e2
                      e4 = popVar $ popVar e3
                      v = localVar "_in_binary" $ instToDataM EMPTY_
                      e5 = pushVar v e4
                      s = addInst <$> d1 <*> d2
                      t = addInst <$> s <*> d3
                      u = instToDataM <$> ezOpToDataM o
ezExprToDataM (EzInt i s) e = (Just $ instToDataM
                            $ M.PUSH (MTpComparableType MTpInt) (MInt $ LInt i), e')
                where v = localVar "_in_int" $ instToDataM M.EMPTY_
                      e' = pushVar v e
ezExprToDataM (EzBool b) e = (Just $ instToDataM
                           $ M.PUSH (MTpComparableType MTpBool) (f b), e')
                where v = localVar "_in_bool" $ instToDataM M.EMPTY_
                      e' = pushVar v e
                      f :: Bool -> DataM
                      f True = MTrue
                      f False = MFalse
ezExprToDataM (EzVar "storage") e =
    (Just inst', e'')
    where (Just d1, e', b) = dupVar "storage" e
          (Just d2, e'') = mvVar "storage" e'
          inst' | b = addInst d1 $ addInst d2 $ instToDataM M.CDR
                | otherwise = addInst d1 d2
ezExprToDataM (EzCall x a) e = ezExprToDataM_ x a e
ezExprToDataM (EzVar x) e
  = case lookupVar x e of
    Just rv -> if scopeR rv == PARAM then
                 dupAndMvVar rv e
               else
                 ezExprToDataM (expr rv) e
    Nothing -> (Just $ MInstruction [], e)
ezExprToDataM _ e = (Just $ MInstruction [], e)

ezExprToDataM_ :: EzExpr -> [EzArgument] -> Env -> (Maybe DataM, Env)
ezExprToDataM_ (EzTez "fail") _ e = (Just $ instToDataM M.FAIL, e)
ezExprToDataM_ (EzTez "amount") _ e =
    (Just $ instToDataM M.AMOUNT
    , pushVar v e)
        where v = amountVar "amount"
ezExprToDataM_ (EzTez "balance") _ e =
    (Just $ instToDataM M.BALANCE
    , pushVar v e)
        where v = balanceVar "balance"
ezExprToDataM_ (EzTez "unit") _ e =
    (Just $ instToDataM M.UNIT
    , pushVar v e)
        where v = VI tmpVar MTpUnit (instToDataM M.UNIT) LOCAL
ezExprToDataM_ (EzTez "tez") [EzArgExpr (EzInt i n)] e =
    (Just $ dataM v
    , pushVar v e)
   where v = tezVar tmpVar n
ezExprToDataM_ _ _ e = (Just $ MInstruction [], e)

dupAndMvVar :: RawVarInfo -> Env -> (Maybe DataM, Env)
dupAndMvVar v e = (t, e'')
  where (d1, e', b) = dupVar (vNameR v) e
        (d2, e'') = mvVar (vNameR v) e'
        s = addInst <$> d1 <*> d2
        t | b && scopeR v == PARAM
              = addInst <$> s <*> (Just $ dataM $ head (stack e''))
          | b && scopeR v == STORAGE
              = addInst <$> s <*> (Just $ dataM $ head (stack e''))
          | otherwise = s

amountRaw :: VarName -> EzExpr -> RawVarInfo
amountRaw n p = RV n p AMT

amountVar :: VarName -> VarInfo
amountVar n = VI n
                 (MTpComparableType MTpTez)
                 (instToDataM M.AMOUNT)
                 AMT

balanceRaw :: VarName -> EzExpr -> RawVarInfo
balanceRaw n p = RV n p BAL

balanceVar :: VarName -> VarInfo
balanceVar n = VI n
                 (MTpComparableType MTpTez)
                 (instToDataM M.BALANCE)
                 BAL

tezRaw :: VarName -> EzExpr -> RawVarInfo
tezRaw v p = RV v p LOCAL

tezVar :: VarName -> String -> VarInfo
tezVar n t = VI n
                tp
                (instToDataM $ M.PUSH tp $ MTez $ LStr t)
                LOCAL
           where tp = MTpComparableType MTpTez

localRaw :: VarName -> EzExpr -> RawVarInfo
localRaw n p = RV n p LOCAL

localVar :: VarName -> DataM -> VarInfo
localVar n d = VI n M.MTpUnknow d LOCAL

storageRaw :: VarName -> EzExpr -> RawVarInfo
storageRaw v p = RV v p STORAGE

paramRaw :: VarName -> EzExpr -> RawVarInfo
paramRaw v p = RV v p PARAM

storeVar :: RawVarInfo -> Env -> Env
storeVar v e = e { vars = H.insert (vNameR v) v (vars e)}

ezOpToDataM :: EzOp -> Maybe Instruction
ezOpToDataM EzLessThan = Just M.CMPLT
ezOpToDataM EzGreaterThan = Just M.CMPGT
ezOpToDataM EzEquality = Just M.CMPEQ
ezOpToDataM EzGreaterThanEquals = Just M.CMPGE
ezOpToDataM EzLessThanEquals = Just M.CMPLT
ezOpToDataM EzNotEquals = Just M.NEQ
ezOpToDataM EzXor = Just M.XOR
ezOpToDataM EzMultiply = Just M.MUL
ezOpToDataM EzPlus = Just M.ADD
ezOpToDataM EzMinus = Just M.SUB
ezOpToDataM EzDivide = Just M.DIV
ezOpToDataM EzAnd = Just M.AND
ezOpToDataM EzOr = Just M.OR
ezOpToDataM EzNot = Just M.NOT
{-ezOpToDataM (EzExponent)-}
{-ezOpToDataM (EzBinaryOr)-}
{-ezOpToDataM (EzBinaryAnd)-}
ezOpToDataM x = error $ show x

pushVar :: VarInfo -> Env -> Env
pushVar i e = Env s (stackBias e) (vars e)
            where s = i : stack e

dupVar :: VarName -> Env -> (Maybe DataM, Env, Bool)
dupVar n e@(Env s b v) =
      if idx /= -1 then
        (Just d, e', a)
      else
        (Nothing, e, a)
      where e' = Env s' b v
            d = if idx == 0 then
                  instToDataM M.DUP
                else
                  instToDataM $ M.DIP idx $ instToDataM M.DUP
            (idx, s', a) = dupVar' s b 0
            dupVar' :: [VarInfo] -> Int -> Int -> (Int, [VarInfo], Bool)
            dupVar' [] b i = (-1, [], False)
            dupVar' (x:xs) b i
              | vName x == n && length s - b - 1 > i
                  = (i, x:x:xs, False)
              | vName x == n
                  = (i, x:x:xs, True)
              | length s - b - 1 > i
                  = let (di, dv, db) = dupVar' xs b (i+1)
                      in (di, x:dv, db)
              | otherwise
                  = let (di, dv, db) = dupVar' xs b i
                      in (di, x:dv, db)

mvVar :: VarName -> Env -> (Maybe DataM, Env)
mvVar n e@(Env s b v) = case target of
                        Just (x, y) ->
                          if x == 0 then
                            (Just $ inst 0, e')
                          else
                            (Just $ inst x, e')
                        Nothing -> (Nothing, e)
      where e' = Env stack' b v
            inst :: Int -> DataM
            inst 0 = MInstruction []
            inst 1 = instToDataM M.SWAP
            inst i = addInst
                       (instToDataM $ M.DIP (i-1) (instToDataM M.SWAP))
                       $ inst (i-1)
            stack' :: [VarInfo]
            stack' = case target of
                       Just (x, y) -> y:s'
                       Nothing -> s'
            (target, s') = findVar' s 0
            findVar' :: [VarInfo] -> Int -> (Maybe (Int, VarInfo), [VarInfo])
            findVar' [] i = (Nothing, [])
            findVar' (x:xs) i
              | vName x == n
                  = (Just (i,x), xs)
              | length s - b - 2 > i
                  = let (di, dv) = findVar' xs (i+1)
                      in (di, x:dv)
              | otherwise
                  = let (di, dv) = findVar' xs i
                      in (di, x:dv)

popVar :: Env -> Env
popVar (Env s b v) = Env (drop 1 s) b v

binaryOpOrder :: VarInfo -> VarInfo -> Env -> (Maybe DataM, Env)
binaryOpOrder l r e@(Env (x:y:xs) b v)
      = (Just $ instToDataM M.SWAP, e { stack = y:x:xs })
    where leftVName = vName l
          rightVName = vName r
