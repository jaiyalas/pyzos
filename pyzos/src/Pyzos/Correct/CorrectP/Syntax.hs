module Pyzos.Correct.CorrectP.Syntax where
--
import Pyzos.Data.DataP
import Pyzos.Correct.Error
--
import Debug.Trace
--
import Control.Monad.State
import Data.List (last, init)
--
-- | proceed checking and resolve state monad
runSyntaxCheck :: EzModule -> (EzModule, [PzError])
runSyntaxCheck ezm = runState (syntaxCheck ezm) []
-- | proceed checking with state monad
syntaxCheck :: EzModule -> PzErrorM EzModule
syntaxCheck m = -- preprocess
                ( return
                $ wellReturnedM
                $ unsafeTezFunComplete
                $ m )
   -- syntax checkings
   >>= checkLayout
   >>= checkStorageSpec
   >>= checkTypeTerm
   >>= return
--
-- pre-process:
unsafeTezFunComplete :: EzModule -> EzModule
unsafeTezFunComplete (EzModule xx) = EzModule (fmap unsafeTezFunCompleteS xx)
--
unsafeTezFunCompleteS :: EzStatement -> EzStatement
unsafeTezFunCompleteS (EzFun name param e1 ss) =
   EzFun name param (unsafeTezFunCompleteE e1) (fmap unsafeTezFunCompleteS ss)
unsafeTezFunCompleteS (EzAssign xs e) = EzAssign xs (unsafeTezFunCompleteE e)
unsafeTezFunCompleteS (EzAnnotatedAssign et x je) =
   EzAnnotatedAssign et x (fmap unsafeTezFunCompleteE je)
unsafeTezFunCompleteS (EzTezStorage e) = EzTezStorage e
unsafeTezFunCompleteS (EzStmtExpr e) = EzStmtExpr (unsafeTezFunCompleteE e)
unsafeTezFunCompleteS (EzConditional sss ss) =
   EzConditional
      (fmap (\(a,b)-> (unsafeTezFunCompleteE a,fmap unsafeTezFunCompleteS b)) sss)
      (fmap unsafeTezFunCompleteS ss)
unsafeTezFunCompleteS (EzReturn e) = EzReturn (unsafeTezFunCompleteE e)
unsafeTezFunCompleteS EzPass = EzPass
--
unsafeTezFunCompleteE :: EzExpr -> EzExpr
-- f tez.fname(args) ==> tez.fname(f* args)
unsafeTezFunCompleteE (EzCall (EzTez fname) args) =
   EzCall (EzTez fname)
         (fmap (\(EzArgExpr e) -> EzArgExpr (unsafeTezFunCompleteE e)) args)
-- f foo.fname(args) ==> foo.fname(f* args)
unsafeTezFunCompleteE (EzCall e args) = EzCall e $
         (fmap (\(EzArgExpr e) -> EzArgExpr (unsafeTezFunCompleteE e)) args)
-- if a tez is not cover by a function-call
-- then its should be a attribute
-- and thus should be translated into function-call
-- f tez.x ==> tez.x()
unsafeTezFunCompleteE (EzTez fname) = EzCall (EzTez fname) []
-- regular i.e. do nothing but recursion
unsafeTezFunCompleteE (EzCondExpr a b c) =
   EzCondExpr
      (unsafeTezFunCompleteE a)
      (unsafeTezFunCompleteE b)
      (unsafeTezFunCompleteE c)
unsafeTezFunCompleteE (EzBinaryOp op a b) =
   EzBinaryOp op
      (unsafeTezFunCompleteE a)
      (unsafeTezFunCompleteE b)
unsafeTezFunCompleteE (EzUnaryOp op a) =
   EzUnaryOp op (unsafeTezFunCompleteE a)
unsafeTezFunCompleteE (EzDot a b) =
   EzDot (unsafeTezFunCompleteE a) b
unsafeTezFunCompleteE (EzPairT a b) =
   EzPairT (unsafeTezFunCompleteE a) (unsafeTezFunCompleteE b)
unsafeTezFunCompleteE (EzParen a) =
   EzParen (unsafeTezFunCompleteE a)
unsafeTezFunCompleteE x = x
--
-- |
wellReturnedM :: EzModule -> EzModule
wellReturnedM (EzModule xs) =
   EzModule $ fmap wellReturn xs
--
wellReturn :: EzStatement -> EzStatement
wellReturn (EzFun s p e []) = (EzFun s p e [EzPass])
wellReturn (EzFun s p e suite) =
   let ys = fmap wellReturn (init suite)
       zs  = wellEnd (last suite)
   in EzFun s p e (ys++zs)

wellReturn (EzConditional [] es) =
   let ys = fmap wellReturn (init es)
       zs  = wellEnd (last es)
   in EzConditional [] (ys++zs)

wellReturn (EzConditional ((e, ss) : xs) es) =
   let (EzConditional xs' es') = wellReturn (EzConditional xs es)
       ys = fmap wellReturn (init ss)
       zs = wellEnd (last ss)
   in EzConditional ((e, (ys++zs)):xs') es'
wellReturn s = s
--
wellEnd :: EzStatement -> [EzStatement]
wellEnd (EzStmtExpr e) = [EzReturn e]

wellEnd a@(EzAssign (var:_) _) = [a, EzReturn var]
wellEnd a@(EzAnnotatedAssign _ var _) = [a, EzReturn var]

wellEnd ezc@(EzConditional _ _) = [wellReturn ezc]
wellEnd x = [x]
--
-- | checking: program layout: storage + entry
checkLayout :: EzModule -> PzErrorM EzModule
checkLayout x@(EzModule [EzTezStorage _ , EzFun fname _ _ _]) =
   case fname of
      "entry" -> return x
      fn -> withState ((throwNotEntry $ show fn):) (return x)
checkLayout e =
   withState ((throwIllLayout $ show e):) (return e)
--
-- | checking: storage is declared in term of types
checkStorageSpec :: EzModule -> PzErrorM EzModule
checkStorageSpec (EzModule es) =
   (mapM checkStorageSpec' es) >>= (return.EzModule)
--
checkStorageSpec' :: EzStatement -> PzErrorM EzStatement
checkStorageSpec' (EzTezStorage e) =
   checkTypeExpr e >>= (return.EzTezStorage)
checkStorageSpec' e = return e
--
checkTypeExpr :: EzExpr -> PzErrorM EzExpr
checkTypeExpr (EzPairT l r) = do
   l' <- checkTypeExpr l
   r' <- checkTypeExpr r
   return (EzPairT l' r')
checkTypeExpr x@(EzTezT s) = return x
checkTypeExpr e =
   withState ((throwNotType $ show e):) (return e)
--
-- | checking: no type term in function body
checkTypeTerm :: EzModule -> PzErrorM EzModule
checkTypeTerm m@(EzModule [_, EzFun _ _ _ body]) = do
   mapM checkTypeTermInStatement body
   >> return m
--
checkTypeTermInStatement :: EzStatement -> PzErrorM EzStatement
checkTypeTermInStatement (EzFun name args ret body) = do
   body' <- mapM checkTypeTermInStatement body
   return $ (EzFun name args ret body')
checkTypeTermInStatement (EzAssign es e) = do
   es' <- mapM checkNoTypeExpr es
   e'  <- checkNoTypeExpr e
   return $ EzAssign es' e'
checkTypeTermInStatement (EzAnnotatedAssign typesE varE maybeValE) = do
   varE' <- checkNoTypeExpr varE
   case maybeValE of
      Nothing ->
         return $ EzAnnotatedAssign typesE varE' Nothing
      (Just e) -> do
         e' <- checkNoTypeExpr e
         return $ EzAnnotatedAssign typesE varE' (Just e')
checkTypeTermInStatement
   (EzConditional conds elses) =
      let foo (a, b) = do
            a' <- checkNoTypeExpr a
            b' <- mapM checkTypeTermInStatement b
            return (a', b')
      in do
         conds' <- mapM foo conds
         elses' <- mapM checkTypeTermInStatement elses
         return $ EzConditional conds' elses'
--
checkTypeTermInStatement (EzTezStorage e) =
   checkNoTypeExpr e >>= (return.EzTezStorage)
checkTypeTermInStatement (EzStmtExpr e) =
   checkNoTypeExpr e >>= (return.EzStmtExpr)
checkTypeTermInStatement (EzReturn e) =
   checkNoTypeExpr e >>= (return.EzReturn)
checkTypeTermInStatement EzPass = return EzPass
--
checkNoTypeExpr :: EzExpr -> PzErrorM EzExpr
checkNoTypeExpr x@(EzTezT _) =
   withState ((throwIsType $ show x):) (return x)
checkNoTypeExpr x@(EzPairT _ _) =
   withState ((throwIsType $ show x):) (return x)
checkNoTypeExpr (EzCall e args) = do
   e' <- checkNoTypeExpr e
   args' <- mapM checkNoTypeArg args
   return $ EzCall e' args'
checkNoTypeExpr (EzCondExpr et ec ef) = do
   et' <- checkNoTypeExpr et
   ec' <- checkNoTypeExpr ec
   ef' <- checkNoTypeExpr ef
   return $ EzCondExpr et' ec' ef'
checkNoTypeExpr (EzBinaryOp x e1 e2) = do
   e1' <- checkNoTypeExpr e1
   e2' <- checkNoTypeExpr e2
   return $ EzBinaryOp x e1' e2'
checkNoTypeExpr (EzUnaryOp x e) = do
   e' <- checkNoTypeExpr e
   return $ EzUnaryOp x e'
checkNoTypeExpr (EzDot e x) = do
   e' <- checkNoTypeExpr e
   return $ EzDot e' x
checkNoTypeExpr (EzParen e) =
   checkNoTypeExpr e >>= (return.EzParen)
checkNoTypeExpr e = return e
--
checkNoTypeArg :: EzArgument -> PzErrorM EzArgument
checkNoTypeArg (EzArgExpr e) =
   checkNoTypeExpr e >>= (return.EzArgExpr)
