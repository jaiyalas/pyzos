module Pyzos.Correct.CorrectP.Type where
--
import Debug.Trace
import Pyzos.Data.DataP
import Pyzos.Correct.Error
--
import Control.Monad
import Control.Monad.State
--
import qualified Data.HashMap as Map
import Data.Maybe (maybe)
import Data.List (last)
import Data.Bifunctor (bimap)
--
--
data TezType
   -- nullary
   = TezTypeString
   | TezTypeInt
   | TezTypeBool
   | TezTypeNat
   | TezTypeUnit
   | TezTypeTez
   -- unary
   | TezTypeMaybe TezType
   -- binary
   | TezTypeProduct TezType TezType
   | TezTypeSum     TezType TezType
   --
   | PolyType
   | ErrorType
   deriving Show
--
instance Eq TezType where
   TezTypeString == TezTypeString = True
   TezTypeInt == TezTypeInt = True
   TezTypeBool == TezTypeBool = True
   TezTypeNat == TezTypeNat = True
   TezTypeUnit == TezTypeUnit = True
   TezTypeTez == TezTypeTez = True
   -- unary
   (TezTypeMaybe t1) == (TezTypeMaybe t2) = t1 == t2
   -- binary
   (TezTypeProduct t1 u1) == (TezTypeProduct t2 u2) =
      (t1 == t2) && (u1 == u2)
   (TezTypeSum t1 u1) == (TezTypeSum t2 u2) =
      (t1 == t2) && (u1 == u2)
    --
   PolyType == _ = True
   _ == PolyType = True
   _ == _ = False
--
runTypeChecking :: EzModule -> (TezType, Info)
runTypeChecking (EzModule [storage,entry]) =
   runState (typeOfState storage >> typeOfState entry) (Info Map.empty [])

type Ctx a = Map.Map String a
type TypeCtx = Ctx TezType
type TypingM a = State Info a
--
data Info = Info { ctx :: TypeCtx
                 , errs :: [PzError]}
          deriving (Show, Eq)
--
consError :: PzError -> Info -> Info
consError e (Info ctx errs) = Info ctx (e : errs)
consCtx :: String -> TezType -> Info -> Info
consCtx k v (Info ctx errs) = Info (Map.insert k v ctx) errs
--
throwError :: PzError -> TypingM ()
throwError e = modify' (consError e)
--
addTVar :: String -> TezType -> TypingM ()
addTVar k v = modify' (consCtx k v)
--
addTVarE :: EzExpr -> TezType -> TypingM ()
addTVarE (EzVar name) t = addTVar name t
addTVarE e t = return ()
--
lookupTVar :: String -> TypingM TezType
lookupTVar k = do
   ((Map.lookup k) . ctx) <$> get >>=
      (\maybe_v ->
         case maybe_v of
            (Just v) -> return v
            Nothing ->
               throwError (throwUnknownVarType k)
               >> (return ErrorType))
--
--
typeOfState :: EzStatement -> TypingM TezType
typeOfState (EzFun name param retTE body) = do
   insertParams param
   info0 <- get
   let (retT, info1) = runState (typeOfExpr retTE) info0
   put info1
   case retT of
      ErrorType -> do
         throwError ( throwUntypeable $ (show retTE))
         return ErrorType
      t -> do
         addTVar "_in_currentRetT" t
         info2 <- get
         let (finalT, info3) = bimap last id $ runState (mapM typeOfState body) info2
         put info3
         case finalT of
            ErrorType -> do
               throwError ( throwUntypeable
                          $ "Cannot reduce a type from the function body of: "
                          ++ name )
               return ErrorType
            tt -> do
               if tt == finalT
                  then return finalT
                  else do
                     throwError ( throwImmatchableType
                                $ "From function body of: " ++ name
                                ++ "\n\t[EXPECTED]: " ++ (show retT)
                                ++ "\n\t[ACTUALLY]: " ++ (show tt) )
                     return ErrorType
--
typeOfState (EzConditional pESs elseSs) = do
   (bs, tss) <- typeOfGuards pESs
   let ts = concat tss
   info0 <- get
   let (elseTs, info1) = runState (mapM typeOfState elseSs) info0
   let elseT = last elseTs
   put info1
   if all (== elseT) ts
      then return elseT
      else do
         throwError ( throwInconsist
                    $ "Branches: "
                    ++ showpESs (zip bs tss)
                    ++ "\n\t  <else>\t->  " ++ (show elseT)
                    ++ "\n\tare inconsistent")
         return ErrorType
   where
      showpESs :: (Show a, Show b) => [(a,[b])] -> String
      showpESs [] = ""
      showpESs ((a,bs) : xs) =
         "\n\t  "++ (show a) ++ "\t->  "
         ++ (show $ last bs)
         ++ showpESs xs
--
typeOfState (EzAssign varEs valE) = do
   info0 <- get
   let (valT, info1) = runState (typeOfExpr valE) info0
   put info1
   case valT of
      ErrorType -> do
         throwError ( throwUntypeable
                    $ "Cannot reduce a type from: "
                    ++ (show valE) )
         return ErrorType
      t -> do
         mapM ((flip addTVarE) t) varEs
         return t
--
typeOfState (EzAnnotatedAssign tE varE (Just valE)) = do
   info0 <- get
   let (valT, info1) = runState (typeOfExpr valE) info0
   let (tT, info2) = runState (typeOfExpr tE) info0
   put info2
   case (valT, tT) of
      (ErrorType, _) -> do
         throwError ( throwUntypeable
                    $ "Cannot reduce a type from: "
                    ++ (show valE) )
         return ErrorType
      (_, ErrorType) -> do
         throwError ( throwUntypeable
                    $ "Cannot reduce a type from: "
                    ++ (show tE) )
         return ErrorType
      (valT', tT') -> if valT' == tT'
                        then do
                           addTVarE varE valT'
                           return valT'
                        else do
                           throwError ( throwImmatchableType
                                      $ "From: " ++ (show varE)
                                      ++ " = " ++ (show valE)
                                      ++ "\n\t[EXPECTED]: " ++ (show tT')
                                      ++ "\n\t[ACTUALLY]: " ++ (show valT'))
                           return ErrorType
typeOfState (EzAnnotatedAssign tE varE Nothing) = do
   info0 <- get
   let (tT, info2) = runState (typeOfExpr tE) info0
   case tT of
      ErrorType -> do
         throwError ( throwUntypeable
                    $ "Cannot reduce a type from: " ++ (show tE))
         return ErrorType
      t -> do
         addTVarE varE t
         return t
typeOfState (EzTezStorage e) = do
   info0 <- get
   let (et, info1) = runState (typeOfExpr e) info0
   put info1
   addTVar "storage" et
   return et
typeOfState (EzStmtExpr   e) = typeOfExpr e
typeOfState (EzReturn     e) = do
   info0 <- get
   let (et, info1) = runState (typeOfExpr e) info0
   put info1
   retT <- lookupTVar "_in_currentRetT"
   if et == retT
      then return et
      else do
         throwError ( throwImmatchableType
                    $ "From: " ++ (show e)
                    ++ "\n\t[EXPECTED]: " ++ (show retT)
                    ++ "\n\t[ACTUALLY]: " ++ (show et) )
         return ErrorType
-- typeOfState EzPass = return PolyType
typeOfState EzPass = lookupTVar "_in_currentRetT"
--
--
insertParams :: EzParameter -> TypingM ()
insertParams (EzParam (vname, tE)) = do
   info0 <- get
   let (tT, info1) = runState (typeOfExpr tE) info0
   put info1
   addTVar vname tT
insertParams (EzParamPair pl pr) =
   insertParams pl >> insertParams pr
--
typeOfGuards :: [(EzExpr, [EzStatement])] -> TypingM ([TezType], [[TezType]])
typeOfGuards [x] = do
   (a, bs) <- typeOfGuard x
   return ([a], [bs])
typeOfGuards (x:xs) = do
   (a, bs) <- typeOfGuard x
   (as, bss) <- typeOfGuards xs
   return (a:as, bs:bss)

typeOfGuard :: (EzExpr, [EzStatement]) -> TypingM (TezType, [TezType])
typeOfGuard (bE, xs) = do
   info0 <- get
   let (bT, info1) = runState (typeOfExpr bE) info0
   put info1
   if bT == TezTypeBool
      then do
         xsT <- mapM typeOfState xs
         return (bT, xsT)
      else throwError ( throwIsNotBool
                      $ "From: " ++ show bE
                      ++ "\n\tis not a boolean" )
            >> return (ErrorType, [ErrorType])
--
--
typeOfExpr :: EzExpr -> TypingM TezType
typeOfExpr (EzVar str) = lookupTVar str
typeOfExpr (EzInt _ _) = return TezTypeInt
typeOfExpr (EzBool _) = return TezTypeBool
typeOfExpr (EzString _) = return TezTypeString
--
typeOfExpr (EzTezT tname) = return $
   case tname of
      "Int" ->    TezTypeInt
      "Nat" ->    TezTypeNat
      "Tez" ->    TezTypeTez
      "Unit" ->   TezTypeUnit
      "String" -> TezTypeString
      "Option" -> TezTypeMaybe PolyType
      "Or" ->     TezTypeSum PolyType PolyType
      _ -> ErrorType
typeOfExpr (EzTez funname) = return $
   case funname of
      "int" ->    TezTypeInt
      "nat" ->    TezTypeNat
      "tez" ->    TezTypeTez
      "unit" ->   TezTypeUnit
      "string" -> TezTypeString
      "some" ->   TezTypeMaybe PolyType
      "none" ->   TezTypeMaybe PolyType
      "left" ->   TezTypeSum PolyType PolyType
      "right" ->  TezTypeSum PolyType PolyType
      "amount" -> TezTypeTez
      "balance" -> TezTypeTez
      "fail" -> PolyType
      _ -> ErrorType
typeOfExpr (EzPairT le re) = do
   info0 <- get
   let (le', info1) = runState (typeOfExpr le) info0
       (re', info2) = runState (typeOfExpr re) info1
   put info2
   return $ TezTypeProduct le' re'
typeOfExpr (EzParen e) = typeOfExpr e
typeOfExpr c@(EzCondExpr tExpr cdExpr fExpr) = do
   info0 <- get
   let (cdType, info1) = runState (typeOfExpr cdExpr) info0
   let (tType,  info2) = runState (typeOfExpr tExpr)  info1
   let (fType,  info3) = runState (typeOfExpr fExpr)  info2
   put info3
   case (cdType, tType == fType) of
      (TezTypeBool, True) -> return tType
      (TezTypeBool, False ) -> do
         throwError ( throwInconsist
                    $ "From: " ++ show c
                    ++ "\n\t[BranchT]: " ++ show tType
                    ++ "\n\t[BeanchF]: " ++ show fType )
         return ErrorType
      otherwise -> do
         throwError (throwIsNotBool $ show cdExpr)
         return ErrorType
-- assumption: only "tez.x" is allowed
-- this non-tez should be checked in syntax checking
typeOfExpr (EzDot x attr) = typeOfExpr (EzTez attr)
--
typeOfExpr (EzCall funE args) = do
   let funParamT = getFunParamTypes funE
   argTs <- mapM (typeOfExpr.argExpr) args
   passb <- and <$> (mapM typeMatching $ zip funParamT argTs)
   -- passb : if arg-param matchable
   if passb
      then fillFunArgs funE argTs
      else return ErrorType
--
typeOfExpr (EzUnaryOp EzNot argE) = do
   info0 <- get
   let (argT, info1) = runState (typeOfExpr argE) info0
   put info1
   case argT of
      TezTypeBool -> return TezTypeBool
      t -> do
         throwError (throwIsNotBool $ show argE)
         return ErrorType
-- the opE here can be only one of `EzMinus` or `EzPlus`
typeOfExpr (EzUnaryOp opE argE) = do
   info0 <- get
   let (argT, info1) = runState (typeOfExpr argE) info0
   put info1
   case argT of
      TezTypeInt -> return TezTypeInt
      TezTypeNat -> return TezTypeNat
      TezTypeTez -> return TezTypeTez
      t -> do
         throwError ( throwIsNotNum
                    $ "From: " ++ (show argE)
                    ++ " has type: " ++ (show argT))
         return ErrorType
--
typeOfExpr (EzBinaryOp opE leftE rightE) = do
   info0 <- get
   let (leftT, info1) = runState (typeOfExpr leftE) info0
       (rightT, info2) = runState (typeOfExpr rightE) info1
   put info2
   case biopmatch opE leftT rightT of
      ErrorType -> do
         throwError ( throwInconsist
                    $ "Types \n\t  \"" ++ show leftT
                    ++ "\" \n\tand \n\t  \"" ++ show rightT
                    ++ "\"\n\tare inconsistent for " ++ show opE )
         return ErrorType
      retT -> return retT
--
biopmatch :: EzOp -> TezType -> TezType -> TezType
-- isComparable
biopmatch EzLessThan leftT rightT          = isComparable leftT rightT
biopmatch EzGreaterThan leftT rightT       = isComparable leftT rightT
biopmatch EzEquality leftT rightT          = isComparable leftT rightT
biopmatch EzGreaterThanEquals leftT rightT = isComparable leftT rightT
biopmatch EzLessThanEquals leftT rightT    = isComparable leftT rightT
biopmatch EzNotEquals leftT rightT         = isComparable leftT rightT
-- isBittable
biopmatch EzShiftLeft leftT rightT  = isBittable leftT rightT
biopmatch EzShiftRight leftT rightT = isBittable leftT rightT
biopmatch EzBinaryOr leftT rightT   = isBittable leftT rightT
biopmatch EzBinaryAnd leftT rightT  = isBittable leftT rightT
biopmatch EzXor leftT rightT        = isBittable leftT rightT
-- isArithmetic
biopmatch EzMultiply leftT rightT = isComputable leftT rightT
biopmatch EzPlus leftT rightT     = isComputable leftT rightT
biopmatch EzMinus leftT rightT    = isComputable leftT rightT
biopmatch EzDivide leftT rightT   = isComputable leftT rightT
-- isLogical
biopmatch EzAnd leftT rightT = isLogical leftT rightT
biopmatch EzOr leftT rightT  = isLogical leftT rightT

-- (<<), etc...
isBittable   :: TezType -> TezType -> TezType
isBittable TezTypeInt TezTypeInt = TezTypeInt
isBittable TezTypeNat TezTypeInt = TezTypeNat
isBittable TezTypeInt TezTypeNat = TezTypeInt
isBittable TezTypeNat TezTypeNat = TezTypeNat
isBittable _ _ = ErrorType
-- AND, OR
isLogical    :: TezType -> TezType -> TezType
isLogical TezTypeBool TezTypeBool = TezTypeBool
isLogical _ _ = ErrorType
-- (<), (<=), etc ...
isComparable :: TezType -> TezType -> TezType
isComparable TezTypeInt TezTypeInt = TezTypeBool
isComparable TezTypeNat TezTypeNat = TezTypeBool
isComparable TezTypeTez TezTypeTez = TezTypeBool
isComparable TezTypeString TezTypeString = TezTypeBool
isComparable _ _ = ErrorType
--
isComputable :: TezType -> TezType -> TezType
isComputable TezTypeInt TezTypeInt = TezTypeInt
isComputable TezTypeNat TezTypeInt = TezTypeInt
isComputable TezTypeInt TezTypeNat = TezTypeInt
isComputable TezTypeNat TezTypeNat = TezTypeNat
isComputable TezTypeTez TezTypeTez = TezTypeTez
isComputable _ _ = ErrorType

-- | types matching
typeMatching :: (TezType, TezType) -> TypingM Bool
typeMatching (t1,t2)
   | t1 == t2  = return True
   | otherwise = do
      throwError ( throwImmatchableType
                 $  "[EXPECTED]: " ++ (show t1)
                 ++ "\n\t[ACTUALLY]: " ++ (show t2))
      return False
--
fillFunArgs :: EzExpr -> [TezType] -> TypingM TezType
fillFunArgs (EzTez "unit")    [] = return TezTypeUnit
fillFunArgs (EzTez "fail")    [] = return PolyType
fillFunArgs (EzTez "amount")  [] = return TezTypeTez
fillFunArgs (EzTez "balance") [] = return TezTypeTez
--
fillFunArgs (EzTez "int")    [TezTypeInt] = return TezTypeInt
fillFunArgs (EzTez "nat")    [TezTypeInt] = return TezTypeNat
fillFunArgs (EzTez "tez")    [TezTypeInt] = return TezTypeTez
fillFunArgs (EzTez "string") [TezTypeString] = return TezTypeString
--
fillFunArgs (EzTez "some")   [argT] = return $ TezTypeMaybe argT
fillFunArgs (EzTez "left")   [argT] = return $ TezTypeSum argT PolyType
fillFunArgs (EzTez "right")  [argT] = return $ TezTypeSum PolyType argT
--
fillFunArgs f argTs  = do
   throwError ( throwImmatchableType
              $ "From: " ++ (show f)
              ++ "\n\t[EXPECTED]: " ++ (show $ getFunParamTypes f)
              ++ "\n\t[ACTUALLY]: " ++ (show argTs) )
   return ErrorType
--
getFunParamTypes :: EzExpr -> [TezType]
getFunParamTypes (EzTez "unit")    = []
getFunParamTypes (EzTez "fail")    = []
getFunParamTypes (EzTez "amount")  = []
getFunParamTypes (EzTez "balance") = []
getFunParamTypes (EzTez "int")     = [TezTypeInt]
getFunParamTypes (EzTez "nat")     = [TezTypeInt]
getFunParamTypes (EzTez "tez")     = [TezTypeInt]
getFunParamTypes (EzTez "string")  = [TezTypeString]
getFunParamTypes (EzTez "some")    = [PolyType]
getFunParamTypes (EzTez "left")    = [PolyType]
getFunParamTypes (EzTez "right")   = [PolyType]
getFunParamTypes otherwise         = [ErrorType]
--
