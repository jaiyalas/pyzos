module Pyzos.Correct.Error where
--
import Control.Monad.State
--
data PzError
   = SyntaxError SyntaxErrorType String
   | TypeError TypeErrorType String
   deriving (Eq, Show)
--
data TypeErrorType
   = UnknownVarType
   | IsNotBool
   | IsNotNum
   | Inconsist
   | ImmatchableType
   | Untypeable
   deriving (Show, Eq)
--
data SyntaxErrorType
   = SynErrNotEntry
   | SynErrIllLayout
   | SynErrNotType
   | SynErrIsType
   deriving (Show, Eq)
--
type PzErrorM = State [PzError]
--
throwNotEntry  :: String -> PzError
throwIllLayout :: String -> PzError
throwNotType   :: String -> PzError
throwIsType    :: String -> PzError
throwNotEntry  e = SyntaxError SynErrNotEntry e
throwIllLayout e = SyntaxError SynErrIllLayout e
throwNotType   e = SyntaxError SynErrNotType e
throwIsType    e = SyntaxError SynErrIsType e
--
throwUnknownVarType   :: String -> PzError
throwIsNotBool        :: String -> PzError
throwInconsist        :: String -> PzError
throwImmatchableType  :: String -> PzError
throwUnknownVarType  e = TypeError UnknownVarType e
throwIsNotBool       e = TypeError IsNotBool e
throwIsNotNum        e = TypeError IsNotNum  e
throwInconsist       e = TypeError Inconsist e
throwImmatchableType e = TypeError ImmatchableType e
throwUntypeable      e = TypeError Untypeable e
--
showTyErrType :: TypeErrorType -> String
showTyErrType UnknownVarType  = "No Such Variable"
showTyErrType IsNotBool       = "Is NOT a Boolean"
showTyErrType IsNotNum        = "Is NOT a Number"
showTyErrType Inconsist       = "Types Inconsistent"
showTyErrType ImmatchableType = "Cannot Match Types"
showTyErrType Untypeable      = "Is Untypeable"
--
showSynErrType :: SyntaxErrorType -> String
showSynErrType SynErrNotEntry  = "Not an \"entry\" Function"
showSynErrType SynErrIllLayout = "Ill Program Layout"
showSynErrType SynErrNotType   = "Not a Type Term"
showSynErrType SynErrIsType    = "Should NOT be a Type Term"
--
putErrorList :: [PzError] -> IO ()
putErrorList [] = return ()
putErrorList (x:xs) = do
   putStrLn $ showError x
   putErrorList xs
--
showError :: PzError -> String
showError (SyntaxError t s) =
   "\ESC[1;37;41m\STX" ++ "[SYNTAX ERROR]" ++ "\ESC[0m\STX" ++
   "\ESC[1;32m\STX " ++ (showSynErrType t) ++ "\ESC[0m\STX" ++
   "\n\t\ESC[36m\STX" ++ s ++ "\ESC[0m\STX"
showError (TypeError t s) =
   "\ESC[1;37;45m\STX" ++ "[TYPE ERROR]" ++ "\ESC[0m\STX" ++
   "\ESC[1;32m\STX " ++ (showTyErrType t) ++ "\ESC[0m\STX" ++
   "\n\t\ESC[36m\STX" ++ s ++ "\ESC[0m\STX"
--
putParseError :: (Show a) => a -> IO ()
putParseError msg = do
   putStrLn $ "\ESC[1;37;41m\STX" ++ "[PARSE ERROR]" ++ "\ESC[0m\STX"
   putStrLn $ "\n\t" ++ (show msg)
--
putUPSError :: IO ()
putUPSError = do
   putStrLn $ "\ESC[1;37;41m\STX" ++
              "[UNSUPPORTED PYTHON SYNTAX ERROR]" ++ "\ESC[0m\STX"
--
