module Pyzos.Correct
   ( module Pyzos.Correct.CorrectP.Syntax
   , module Pyzos.Correct.CorrectP.Type
   , module Pyzos.Correct.Error
   -- * Utils for manipulating AST
   -- ** pre-translating operations
   , process
   , proceedPreprocess
   , proceedSyntacCheck
   , proceedTypeCheck
   -- ** display msg from result
   , ezM
   , ezS
   , ezT
   )
   where
--
import Pyzos.Correct.CorrectP.Syntax
import Pyzos.Correct.CorrectP.Type
import Pyzos.Correct.Error
--
import Pyzos.Data.DataP
--
import Control.Monad
import Language.Python.Common.AST (ModuleSpan)
--
-- | Ugly but working pipeline of three pre-translating checking.
-- The input module must be pre-processed.
process :: (Maybe EzModule) -> IO (Maybe EzModule)
process ezm = do
   mezm <- ezM ezm
   case mezm of
      Nothing -> return Nothing
      (Just x) -> do
         let mezm2 = runSyntaxCheck x
         mezm3 <- ezS mezm2
         case mezm3 of
            Nothing -> return Nothing
            (Just y) -> do
               let (t, info) = runTypeChecking y
               ter <- ezT (t, info)
               case ter of
                  Nothing -> return Nothing
                  (Just z) -> return (Just y)
--
proceedPreprocess :: ModuleSpan -> Maybe EzModule
proceedPreprocess mspan = ezModule mspan
--
proceedSyntacCheck :: EzModule -> Maybe (EzModule, [PzError])
proceedSyntacCheck ezm =
   let x@(ezm', errs) = runSyntaxCheck ezm
   in if null errs then (Just x) else Nothing
--
proceedTypeCheck :: EzModule -> Maybe (TezType, Info)
proceedTypeCheck ezm =
   let x@(tt, info) = runTypeChecking ezm
   in if null (errs info) then (Just x)else Nothing
--
ezM :: (Maybe EzModule) -> IO (Maybe EzModule)
ezM (Just ezm) = do
   printSafeM ezm
   return (Just ezm)
ezM Nothing = do
   putUPSError
   return Nothing
--
ezS :: (EzModule, [PzError]) -> IO (Maybe EzModule)
ezS (ezm, []) = do
   printSafeS ezm
   return (Just ezm)
ezS (ezm, errs) = do
   putErrorList errs
   return Nothing
--
ezT :: (TezType, Info) -> IO (Maybe TezType)
ezT (t, Info _ []) = do
   printSafeT t
   return (Just t)
ezT (t, Info _ errs) = do
   putErrorList errs
   return Nothing
--
printSafeM :: EzModule -> IO ()
printSafeM ezm = do
   putStrLn $ "....."++"\ESC[1;37;42m\STX" ++ "[Preprocessed]" ++ "\ESC[0m\STX"
--
printSafeS :: EzModule -> IO ()
printSafeS ezm = do
   putStrLn $ "....."++"\ESC[1;37;42m\STX" ++ "[Syntax Checked]" ++ "\ESC[0m\STX"
--
printSafeT :: TezType -> IO ()
printSafeT t = do
   putStrLn $ "....."++"\ESC[1;37;42m\STX" ++ "[Type Checked]" ++ "\ESC[0m\STX"
--
