
module Pyzos.Arg where

import Options.Applicative
import Data.Monoid

data Arg = Gen
         { inputFile  :: String
         , outputFile :: Maybe String
         }

cmdLineOut :: Parser Arg
cmdLineOut = subparser
                ( command "Gen"
                    ( info (helper <*> genArg) fullDesc)
                <> help "Use '--help' to see more detail"
                )

genArg :: Parser Arg
genArg = Gen
        <$> infileParser
        <*> outfileParser

infileParser :: Parser String
infileParser = strOption
                   ( long "inFilePath"
                   <> short 'i'
                   <> metavar "INPUT_FILE_PATH"
                   <> help "input file"
                   )

outfileParser :: Parser (Maybe String)
outfileParser = optional
              $ strOption
                   ( long "outFilePath"
                   <> short 'o'
                   <> metavar "OUTPUT_FILE_PATH"
                   <> help "if no specify the output file, the output will be showed on stdout"
                   )
