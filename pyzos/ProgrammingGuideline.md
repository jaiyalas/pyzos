
# Pyzos Programming Guideline

Please read

> pyzos/pyzos/data/* 

for example program.

## Current version: 0.1.2

### program layout

```
storage: [<type>]

def entry(<args>: <types>) -> <type>:
   <function body>
```

**NOTICE**: (1) `pair` can be used in `storage` only! (2) the only legal type functor here is `pair` or `tez.EzPairT`

### supported native syntax

- variable declaration
- basic control-flow
   - branch

### writing style

- The `entry` function
   - each contract must provide a entry function  
   - mush named `entry`
   - no declarators
   - has explicit function type
      - if not, an `unit` will be filled in
- Strong and static type system
- All data types and operations are defined in `tez`
   - e.g. `tez.tez(10)` or `tez.fail()`
- Explicit type for global storage

## Past edition

### 0.1.1

**program layout**

```
storage: public(<type>)

def entry(<args>: <types>) -> <type>:
   <function body>
```

**writing style**

- the main function mush be named as `entry`
   - no declarators
- no global variable
   - except storage type declaration
- no local function
- explicit type for entry function
   - even `tez.Unit -> tez.Unit` is necessary

### 0.1.0

```
storage: public(<type>)
<global variable>

@public
@entry
def entry(<args>: <types>) -> <type>:
   <function body>

<local function definition>
```
