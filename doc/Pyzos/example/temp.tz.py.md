Given a simple format of Michelson.

```
parameter <T>;
return    <U>;
storage   <S>;
code { <Term> };
```

One could know that `Term` has type as `(T, S) -> (U, S)`.

There are two important parts in a Michelson program.
(1) implicit storage
(2) explicit code

The code is relatively easy,

```python
@public
@entry
def entry(<T>) -> <U>:
   <Term>
```

The storage, however, can be considered in sevaral ways,

```python
@public
storage: <S>
```

or

```python
storage: public(<S>)
```

or

```python
@public
@storage
<vname>: <S>
```
