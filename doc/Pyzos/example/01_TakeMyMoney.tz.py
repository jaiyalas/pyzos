storage: [tez.Unit]

def entry(_kh: tez.Key_hash) -> tez.Unit:
    # account :: tez.Contract(tez.Unit)
    account = tez.default_account(_kh)
    money = tez.tez(1.00)
    tez.tansfer_tokens(money, account)
    return unit

# parameter key_hash;
# return unit;
# storage unit;
# code { CAR; DEFAULT_ACCOUNT; # Create an account for the recipient of the funds
#        DIP{UNIT};             # Push a value of the storage type below the contract
#        PUSH tez "1.00";       # The person can have a ꜩ
#        UNIT;                 # Push the contract's argument type
#        TRANSFER_TOKENS;      # Run the transfer
#        PAIR };                # Cleanup and put the return values
