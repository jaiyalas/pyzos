# Features To Do for Pyzos

***Is file is sickly corrupted.*** Please waiting for update.

## native syntax to do

- basic control-flow
   - loop
- first class function
   - closure
- higher-order function   
- decorator
- assertion
- self
- function definition
- **NO CLASS**

## Core data types

```
list (t)
set (t)
map (k) (t)
big_map (k) (t)
```

## Core instructions

### Control structures

```
{ (I :: [ 'A -> 'B ]) ; (C :: [ 'B -> 'C ]) } :: 'A   ->   'C
IF (bt :: [ 'A -> 'B ]) (bf :: [ 'A -> 'B ]) :: bool : 'A    ->   'B
LOOP (body :: [ 'A -> bool : 'A ]) :: bool : 'A   ->   'A
LOOP_LEFT (body :: [ 'a : 'A -> (or 'a 'b) : 'A ]) :: (or 'a 'b) : 'A   ->  'b : 'A
DIP (code :: [ 'A -> 'C ]) :: 'b : 'A   ->   'b : 'C
```

### Stack operations

```
DROP :: _ : 'A   ->   'A
DUP :: 'a : 'A   ->   'a : 'a : 'A
SWAP :: 'a : 'b : 'A   ->   'b : 'a : 'A
PUSH 'a (x :: 'a) :: 'A   ->   'a : 'A
LAMBDA 'a 'b code :: 'A ->  (lambda 'a 'b) : 'A
```

### Generic comparison

```
EQ :: int : 'S   ->   bool : 'S
NEQ :: int : 'S   ->   bool : 'S
LT :: int : 'S   ->   bool : 'S
GT :: int : 'S   ->   bool : 'S
LE :: int : 'S   ->   bool : 'S
GE :: int : 'S   ->   bool : 'S
```

## Operations

### Operations on booleans

```
OR :: bool : bool : 'S   ->   bool : 'S
AND :: bool : bool : 'S   ->   bool : 'S
XOR :: bool : bool : 'S   ->   bool : 'S
NOT :: bool : 'S   ->   bool : 'S
```

### Operations on Int/Nat

```
NEG :: int : 'S   ->   int : 'S
NEG :: nat : 'S   ->   int : 'S
ABS :: int : 'S   ->   nat : 'S
ADD :: int : int : 'S   ->   int : 'S
ADD :: int : nat : 'S   ->   int : 'S
ADD :: nat : int : 'S   ->   int : 'S
ADD :: nat : nat : 'S   ->   nat : 'S
SUB :: int : int : 'S   ->   int : 'S
SUB :: int : nat : 'S   ->   int : 'S
SUB :: nat : int : 'S   ->   int : 'S
SUB :: nat : nat : 'S   ->   int : 'S
MUL :: int : int : 'S   ->   int : 'S
MUL :: int : nat : 'S   ->   int : 'S
MUL :: nat : int : 'S   ->   int : 'S
MUL :: nat : nat : 'S   ->   nat : 'S
EDIV :: int : int : 'S   ->   option (pair int nat) : 'S
EDIV :: int : nat : 'S   ->   option (pair int nat) : 'S
EDIV :: nat : int : 'S   ->   option (pair int nat) : 'S
EDIV :: nat : nat : 'S   ->   option (pair nat nat) : 'S
```

```
OR :: nat : nat : 'S   ->   nat : 'S
AND :: nat : nat : 'S   ->   nat : 'S
AND :: int : nat : 'S   ->   nat : 'S
XOR :: nat : nat : 'S   ->   nat : 'S
NOT :: nat : 'S   ->   int : 'S
NOT :: int : 'S   ->   int : 'S
LSL :: nat : nat : 'S   ->   nat : 'S
LSR :: nat : nat : 'S   ->   nat : 'S
COMPARE :: int : int : 'S   ->   int : 'S
COMPARE :: nat : nat : 'S   ->   int : 'S
```

### Operations on strings

```
CONCAT :: string : string : 'S   -> string : 'S
COMPARE :: string : string : 'S   ->   int : 'S
```

### Operations on pairs

none

### Operations on sets

```
skipped
```

### Operations on maps

skipped

### Operations on `big_maps`

skipped

### Operations on optional values

none

### Operations on unions

none

### Operations on lists

```
CONS :: 'a : list 'a : 'S   ->   list 'a : 'S
NIL 'a :: 'S   ->   list 'a : 'S
MAP body :: (list 'elt) : 'A   ->  (list 'b) : 'A
   iff body :: [ 'elt : 'A -> 'b : 'A ]
SIZE :: list 'elt : 'S -> nat : 'S
ITER body :: (list 'elt) : 'A   ->  'A
   iff body :: [ 'elt : 'A -> 'A ]
```

## DS data types

```
timestamp          ## dates in the real world
tez                ## a specific type for manipulating tokens
contract 'param    ## a contract, with the type of its code
address            ## an untyped contract address
operation          ## an internal operation emitted by a contract
key                ## public cryptography key
key_hash           ## the hash of a public cryptography key
signature          ## cryptographic signature
```

### data construction

The ways to construction DS data types are defined
as in Michelson grammar as follows.

```
<timestamp string constant>
<tez       string constant>
<contract  string constant>
<key       string constant>
<key_hash  string constant>
<signature string constant>
```

First of all,
there are limited ways to construct domain specific data.
The most used way is using `PUSH 't data`.
The interesting fact here is, all construction parameter are string.
In other words, `PUSH 't` can be think as a casting function that
coerce a string into type `'t` and put it into the top of storage.

Secondly, we have no direct way to construct `address` and `operation`.

### other notes

Additionally, about **account**:

- `account` = `contract ()` without any code

about **code**

- *code* = `{ op1, op2, ... }` with type as `lambda 't 'u`
- *code* exists in two cases:
   - (in storage): `OP :: ... : lambda _ _ : ... : s`
   - (in code): `OP {... ; <code> ; ... }`
- *code* of type `lambda 't 'u` can be
   - created by `LAMBDA 't 'u <code>`
   - eliminated by `EXEC` (with *code* in storage)
   - translated into *contract* by `CREATE_CONTRACT` (with *code* in storage)

about **operation**

- ???

An interesting observation, in *contract-a-day*,
every contract has return type, and thus seems no returned operation what so ever;
however, in *Michelson spec*, all example have no return type, and then all code
must return a list of operation.

Another thing, some op returns result with an extra `operation`.
But this seems not what happened in *contract-a-day*.

about **lambda** and **global data**

- The *code* of a contract has type as
   - `lambda (pair 'arg 'global) (pair (list operation) 'global)`
   - `(pair 'arg 'global) -> (pair (list operation) 'global)`
   - where `'global` is the type of the original global store given on origination (= beginning)

about **calling convention**

- *calling convention* means `'t -> 'u`
- the cc of a *code* usually has form as
   - `(arg, globals) -> (operations, globals)` or
   - `(arg, globals) -> (list operation, globals)`

## DS data instructions

### timestampes

```
ADD     :: timestamp : int : 'S -> timestamp : 'S
ADD     :: int : timestamp : 'S -> timestamp : 'S
SUB     :: timestamp : int : 'S -> timestamp : 'S
SUB     :: timestamp : timestamp : 'S -> int : 'S
COMPARE :: timestamp : timestamp : 'S   ->   int : 'S
```

### Tez

```
ADD     :: tez : tez : 'S   ->   tez : 'S
SUB     :: tez : tez : 'S   ->   tez : 'S
MUL     :: tez : nat : 'S   ->   tez : 'S
MUL     :: nat : tez : 'S   ->   tez : 'S
EDIV    :: tez : nat : 'S   ->   option (pair tez tez) : 'S
EDIV    :: tez : tez : 'S   ->   option (pair nat tez) : 'S
COMPARE :: tez : tez : 'S -> int : 'S
```

### Operations on contracts

```
MANAGER          :: address : 'S   ->   key_hash option : 'S
MANAGER          :: contract 'p : 'S   ->   key_hash : 'S
CREATE_CONTRACT
   :: key_hash : option key_hash : bool : bool : tez
   : lambda (pair 'p 'g) (pair (list operation) 'g)
   : 'g : 'S
   -> operation : address : 'S
CREATE_CONTRACT { storage 'g ; parameter 'p ; code ... }
   :: key_hash : option key_hash : bool : bool : tez : 'g : 'S
   -> operation : address : 'S
CREATE_ACCOUNT
   :: key_hash : option key_hash : bool : tez : 'S
   -> operation : contract unit : 'S
TRANSFER_TOKENS  :: 'p : tez : contract 'p : 'S   ->   operation : S
SET_DELEGATE     :: option key_hash : 'S   ->   operation : S
BALANCE          :: 'S   ->   tez : 'S
ADDRESS          :: contract _ : 'S   ->   address : 'S
CONTRACT 'p      :: address : 'S   ->   contract 'p : 'S
SOURCE           :: 'S   ->   address : 'S
SELF             :: 'S   ->   contract 'p : 'S
AMOUNT           :: 'S   ->   tez : 'S
IMPLICIT_ACCOUNT :: key_hash : 'S   ->   contract unit : 'S
```

![alt](./DSDTOPs.jpg)

### Special operations

```
STEPS_TO_QUOTA :: 'S   ->   nat : 'S
NOW            :: 'S   ->   timestamp : 'S
```

### Cryptographic primitives

```
HASH_KEY        :: key : 'S   ->   key_hash : 'S
H               :: 'a : 'S   ->   string : 'S
CHECK_SIGNATURE :: key : signature : string : 'S   ->   bool : 'S
COMPARE :: key_hash : key_hash : 'S   ->   int : 'S
```
