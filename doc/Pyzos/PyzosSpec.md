# Pyzos

The *code block* represents what we provide, from Michelson, as library.
Mostly they are types and domain specific operations.

The *bullet* describes syntax from python and corresponding Michelson operations. 

## syntax borrowing

- using **tuple** to replace `tez.Pair(t, u)`

## additional data type

```python
# is isomorphic to native type
tez.String # == str
tez.Int    # == int
tez.Bool   # == bool
# special types
tez.Nat
tez.Unit
# functors
tez.Option(t)
tez.Or(t, u)
# domain specific types
tez.Tez
```

Constructions:

The constructions of first three types can easily be done by using native-syntax. Others are as follows,

```python
tez.nat: Int -> tez.Nat
tez.unit: () -> tez.Unit
tez.some: a -> tez.Option(a)
tez.none: () -> tez.Option(a)
tez.left: a -> tez.Or(a, b)
tez.right: b -> tez.Or(a, b)
tez.Tez: tez.String -> tez.Tez
```

## control structures

```python
tez.fail: () -> ()
```

- using `if...` for branchs
   - `IF` for `bool`
   - `IF_NONE` for `Option`
   - `IF_LEFT`, `IF_RIGHT` for `Or`

## Operations

```python
tez.balance: () -> tez.Tez
tez.amount: () -> tez.Tez
```

- using arithmetic
   - `ADD`, `SUB`, `MUL`, `EDIV` for `int`, `nat` and `tez`
   - `NEG` for `int` and `nat`
   - `ABS` from `int` to `nat`
- using `+` as string concat
- using `[0]` and `[1]` as fst and snd

## comparison

- using `>=` etc for comparison
   - `EQ`, `NEQ`, `LT`, `GT`, `LE`, `GE`
   - for types: `int` `nat` `string` `tez`

The `COMPARE` itself is not useful, but the macros based on it are powerful.

- the `if..` or `...>=...` can be translated in to  comparison macros
   - `CMP{EQ|NEQ|LT|GT|LE|GE}`
   - `IF{EQ|NEQ|LT|GT|LE|GE} bt bf`
   - `IFCMP{EQ|NEQ|LT|GT|LE|GE} bt bf`
